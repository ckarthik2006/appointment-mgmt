import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dynamic-comp',
  templateUrl: './dynamic-comp.component.html',
  styleUrls: ['./dynamic-comp.component.scss']
})
export class DynamicCompComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() index: number;

}
