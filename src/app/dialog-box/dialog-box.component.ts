import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Appointment } from '../shared/appointment';
import * as moment from 'moment';

export interface UsersData {
  name: string;
  id: number;
}

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss']
})
export class DialogBoxComponent implements OnInit {

  ngOnInit(): void {
  }

  action:string;
  local_data:any;
  dateerr : boolean;

  constructor(
    public dialogRef: MatDialogRef<DialogBoxComponent>,
    
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Appointment) {
    console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    console.log(this.local_data);
    this.changedate();
    if(!this.dateerr){
    this.dialogRef.close({event:this.action,data:this.local_data});
  }
  else{
    window.alert('start date should be less than end date');
  }
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  changedate(){
    console.log('inside change date method');
    var startTime = moment(this.local_data.starttime, 'hh:mm a');
       var endTime = moment(this.local_data.endtime, 'hh:mm a');
   
       console.log(startTime+ '---' +endTime);
        
       console.log(endTime.diff(startTime, 'minutes'));

       if ((endTime.diff(startTime, 'minutes')) <= 0) {
        this.dateerr = true;
       }
       else {
         //this.dateErr = false;
         this.dateerr = false;
       }
       console.log('dateErr'+this.dateerr);
    }
 
     // this.dateerr = this.local_data.starttime >= this.local_data.endtime ? false: true;
      //console.log(this.dateerr);
    
  
}
