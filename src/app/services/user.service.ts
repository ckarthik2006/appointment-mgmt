import { Injectable } from '@angular/core';
import { UserDetails } from '../shared/userDetails';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  edituser : boolean ;
  useridseq : number ;
  constructor(private http : HttpClient) { }

  getUsers(): Observable<UserDetails[]> {
    return this.http.get<UserDetails[]>('http://localhost:3000/users');
  }

  getUser(id: String): Observable<UserDetails> {
    return this.http.get<UserDetails>('http://localhost:3000/users/'+ id);
  }

  putUser(user: UserDetails) : Observable<UserDetails> {

    this.edituser = JSON.parse(localStorage.getItem('id')) == null ? false : true ;
    this.useridseq = this.edituser ? JSON.parse(localStorage.getItem('id'))  : 1;
  
    user.userid = this.useridseq;
    this.useridseq++;
    const httpOptions = {
      headers : new HttpHeaders({
        'Content-Type' : 'application/json'
      })
    };
    return this.http.put<UserDetails>('http://localhost:3000/users/' + user.userid, user, httpOptions);
  }

  postUser(user: UserDetails) : Observable<UserDetails> {

    const httpOptions = {
      headers : new HttpHeaders({
        'Content-Type' : 'application/json'
      })
    };
    return this.http.post<UserDetails>('http://localhost:3000/users/' , user, httpOptions);
  }

}
