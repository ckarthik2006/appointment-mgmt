import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserDetails } from '../shared/userDetails';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public jwtHelper: JwtHelperService = new JwtHelperService();
    constructor(private router: Router) {}
    private loggedIn = new BehaviorSubject<boolean>(false); // {1}

  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }

  login(user: UserDetails){
    this.loggedIn.next(true);
    //this.auth.isAuthenticated();
    console.log('On Login redirect via Auth Service');
    this.router.navigate(["viewappt"]);
  }

  logout() {                            // {4}
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }


    /* public isAuthenticated(): boolean {

      const token = JSON.parse(localStorage.getItem('id')) == null ? false : true ;
    console.log('Auth Service');
    console.log(localStorage.getItem('id'));
    console.log(token);
    localStorage.setItem('isAuth', JSON.stringify(token));
      return token;
    } */
  }