import {HttpTestingController, HttpClientTestingModule} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClientModule } from '@angular/common/http';
import { UserDetails } from '../shared/userDetails';

describe('UserService', () => {
  
  let service: UserService;
  let httpMock: HttpTestingController;
  const dummyUsers: UserDetails[] = [{
    name: 'karthik1',
    password: 'admin1',
    guardian_type: 'Spouse',
    guardian_name: 'XXX',
    address: 'chennai',
    citizenship: 'Indian',
    state: 'Puebla',
    country: 'Mexico',
    email_address: 'aa@ff.cc',
    gender: 'Female',
    marital_status: 'Single',
    contact_no: '1111111111',
    date_of_birth: new Date('2020-09-03T12:23:32.596Z'),
    registration_date: new Date('2020-09-03T12:23:32.596Z'),
    time_zone: '',
    blood_type: '',
    country_visited: 'USA',
    citizen_status: '',
    display_name: '',
    supplier_name: '',
    ssn_number: '',
    username: '629625',
    userid: 19,
    id: 19,
    appointments: [
      {
        timezone: 'PST',
        appointmenttype: 'Private',
        startdate: '11-09-2020',
        title: 'Physio ',
        starttime: new Date('10:00 AM'),
        endtime: '11:00 AM',
        location: '',
        comments: ''
      }
    ]
}];
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
            providers: [UserService]
    });
    service = TestBed.get(UserService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('be able to retrieve users from the API via GET', () => {
    
    service.getUsers().subscribe(posts => {
        expect(posts.length).toBe(1);
        expect(posts).toEqual(dummyUsers);
    })
    
    service.getUser('1').subscribe(posts => {
      
      expect(posts).toEqual(dummyUsers[0]);
  }

  
  );
    const request = httpMock.expectOne( `http://localhost:3000/users`);
    expect(request.request.method).toBe('GET');
    request.flush(dummyUsers);
  });

  it('should save the user to the DB', () => {
    const newUsr: UserDetails = dummyUsers[0];
    const service: UserService = TestBed.get(UserService);
    service.postUser(newUsr).subscribe(
      data => expect(data).toEqual(newUsr, 'should return the User'));
    afterEach(() => {
    httpMock.verify();
});
});

it('should update the user in the DB', () => {
  const newUsr: UserDetails = dummyUsers[0];
  const service: UserService = TestBed.get(UserService);
  service.putUser(newUsr).subscribe(
    data => expect(data).toEqual(newUsr, 'should update the User'));
  afterEach(() => {
  httpMock.verify();
});
});

});
