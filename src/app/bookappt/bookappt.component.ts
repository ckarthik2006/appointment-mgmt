import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { UserDetails } from '../shared/userDetails';
import { Observable, of } from 'rxjs';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { Appointment, TimeOption } from '../shared/appointment';
import { DatePipe } from '@angular/common';
import { Moment } from 'moment';
import * as moment from 'moment';
import { DateValidator } from '../shared/DateValidator';
import { externaljs } from 'src/assets/javascript/external';
import { DynamicCompComponent } from '../dynamic-comp/dynamic-comp.component';

@Component({
  selector: 'app-bookappt',
  templateUrl: './bookappt.component.html',
  styleUrls: ['./bookappt.component.scss']
})
export class BookapptComponent implements OnInit {

editappt : boolean = false;
userDetail : UserDetails;
apptObs : Observable<UserDetails>;
apptForm : FormGroup;
appointment : Appointment[] = [];
minDate : Moment;
changeendtime: Date;
changebegintime: Date;
dateValue: Date = new Date();
starttime : Date;
endtime : Date;
dateError : boolean;
//dateErr : boolean;
private _counter = 1;
@ViewChild('container', { read: ViewContainerRef }) container: ViewContainerRef;

dateValidator = new DateValidator();

  constructor(private fb : FormBuilder, private componentFactoryResolver: ComponentFactoryResolver, private userService :UserService, private router : Router, private datePipe: DatePipe) { 
  }

  ngOnInit(): void {

    console.log(localStorage.getItem('id'));
    this.userService.getUser(localStorage.getItem('id')).subscribe(usrs => this.userDetail = usrs);

    const currentYear = moment().year();
    const currMonth = moment().month();
    const currDate = moment().date();
    this.minDate = moment([currentYear , currMonth, currDate ]);
    
    this.createForm();

    this.apptForm.controls['starttime'].valueChanges.subscribe(change => {
      console.log(change); this.starttime = change;
    });
    this.apptForm.controls['endtime'].valueChanges.subscribe(change => {
       this.endtime = change; console.log(this.endtime); /* this.timeVal() */;
    });

  }

  add(): void {

    // create the component factory
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DynamicCompComponent);

    // add the component to the view
    const componentRef = this.container.createComponent(componentFactory);

    // pass some data to the component
    componentRef.instance.index = this._counter++;
  }
  
  timeVal() : void{

    var startTime = moment(startTime, 'hh:mm:ss a');
    var endTime = moment(endTime, 'hh:mm:ss a');

    console.log(endTime.diff(startTime, 'hours'));

    if(endTime.diff(startTime, 'hours') > 0)
    {
      console.log('start time is greater');
    }
    else {
      console.log('end time is greater');
      this.dateError = true;
      //window.alert('end time is greater');
    }
  }
  onendtimechange({ target }) {
    console.log(target.value);
  }
  
  createForm()
  {
      this.apptForm = this.fb.group({
        timezone :['',[ Validators.required]],
  appointmenttype:['',[ Validators.required]],
  startdate:['',[ Validators.required]],
  title: '',
  starttime :['',[ Validators.required]],
  endtime:['',[ Validators.required]],
  location : '',
  comments: '',
      },
      {validators: [this.dateVal('starttime','endtime')]
      });

  }
    
  dateVal(starttime: string, endtime: string) {
    return (group: FormGroup) => {
       let startTimeControl = group.controls[starttime];
       let endTimeControl = group.controls[endtime];

       var startTime = moment(startTimeControl.value, 'hh:mm:ss a');
       var endTime = moment(endTimeControl.value, 'hh:mm:ss a');
   
       console.log(endTime.diff(startTime, 'hours'));

       if ((endTime.diff(startTime, 'hours')) < 0) {
        // this.dateErr = true;
        //console.log(this.dateErr);  
        return {
             dateErr: true
          };
       }
       else {
         //this.dateErr = false;
         console.log('else loop');
       }
    }
 }

  timeChoice = [
    '09:00 AM',
    '10:00 AM',
    '11:00 AM',
    '12:00 PM',
    '01:00 PM'
  ];

  apptChoice = [
    'Public',
    'Private'
  ];

  tzChoice = [
    'CDT',
    'PST',
    'IST'
  ];
  
  
  onSubmit()
  {
    if(this.apptForm.valid){
    console.log('Saving Appt');
    console.log(this.userDetail);
    console.log(typeof this.userDetail.appointments )
    console.log(this.appointment);
    console.log(this.userDetail.appointments);
    this.apptForm.value.startdate = this.datePipe.transform(this.apptForm.value.startdate, 'dd-MM-yyyy');
    this.appointment.push(this.apptForm.value);
    console.log(this.userDetail.appointments?.length);
    if(this.userDetail.appointments == null)
    {
    this.userDetail.appointments = this.appointment;
    console.log('If loop');
  }
  else {
    this.userDetail.appointments.push(this.apptForm.value);
    console.log('else loop');
  }
    console.log(this.apptForm.value);
    console.log(this.userDetail);
    this.userService.putUser(this.userDetail).subscribe(user => this.userDetail = user);
    var useridStr = JSON.stringify(this.userDetail.userid);
    localStorage.setItem('id', useridStr);
externaljs(this.apptForm.value.starttime, this.apptForm.value.endtime, this.userDetail.contact_no, this.userDetail.email_address);
    
    //window.alert('Successfully booked appointment between '+this.apptForm.value.starttime+' & '+this.apptForm.value.endtime+'You will be contacted through '+this.userDetail.contact_no+' or '+this.userDetail.email_address);
    setTimeout(() => {this.router.navigate(["viewappt"])}
    , 2000);
  } else {
    console.log(this.apptForm.errors);
    console.log('Form has errors');
  }
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.apptForm.controls[controlName].hasError(errorName);
  }

}
