import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookapptComponent } from './bookappt.component';

describe('BookapptComponent', () => {
  let component: BookapptComponent;
  let fixture: ComponentFixture<BookapptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookapptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookapptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
