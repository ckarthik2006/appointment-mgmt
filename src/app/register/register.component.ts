import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { UserDetails } from './../shared/userDetails';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Appointment } from '../shared/appointment';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import { Moment } from 'moment';
import * as moment from 'moment';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

edituser : boolean = false;
userDetail : UserDetails;
userObs : Observable<UserDetails>;
regForm : FormGroup;
id : string;
name: string;
username: string;
password: string;
guardian_type : string;
guardian_name: string;
address: string;
citizenship: string;
state: string;
country: string;
email_address: string;
gender: string;
marital_status: string;
contact_no: string;
date_of_birth: Date;
registration_date: Date;
time_zone: string;
blood_type: string;
country_visited: string;
citizen_status: string;
display_name: string;
supplier_name: string;
ssn_number: string;
appointments : Appointment[];
emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
existingApp : Appointment[];
minDate: Moment;
maxDate: Moment;


  constructor(private router : Router, private fb : FormBuilder, private userService :UserService, private auth : AuthService) { 
    
  }
  //private usrDetail : UserDetails
  ngOnInit(): void {

    console.log(JSON.parse(localStorage.getItem('id')));
    this.edituser = JSON.parse(localStorage.getItem('id')) == null ? false : true ;
    console.log(this.edituser);
    this.id = this.edituser ? JSON.parse(localStorage.getItem('id'))  : null; 
    if(this.edituser && this.id != null)
    {
    this.userService.getUser(this.id).subscribe(usrDetail => {this.userDetail = usrDetail; this.existingApp = usrDetail.appointments});
    //console.log(this.userDetail);
    }

      let randomuname = localStorage.getItem('username');
      console.log(randomuname);
      this.username = randomuname;
      
      const currentYear = moment().year();
      const currMonth = moment().month();
      const currDate = moment().date();
      this.minDate = moment([currentYear , currMonth, currDate ]);
      this.maxDate = moment([currentYear , currMonth, currDate ]);

    this.createForm();
    this.regForm.controls.username.disable();
  }
  
  genderChoice = [
    'Male',
    'Female',
    'Others'
  ];

  countryChoice: Array<any> = [
    { name: 'Germany', cities: ['Duesseldorf', 'Leinfelden-Echterdingen', 'Eschborn'] },
    { name: 'Spain', cities: ['Barcelona'] },
    { name: 'USA', cities: ['Downers Grove'] },
    { name: 'Mexico', cities: ['Puebla'] },
    { name: 'China', cities: ['Beijing'] },
  ];

  cities: Array<any>;

  changeCountry(choice) {
    console.log(choice);
    this.cities = this.countryChoice.find(con => con.name == choice).cities;
  }

  createForm(){

    this.regForm = this.fb.group({
      name : ['',[ Validators.required]],
username: [this.username, [ Validators.required]],
password: ['',[ Validators.required]],
guardian_type: ['',[ Validators.required]],
guardian_name: '',
address: ['',[ Validators.required]],
citizenship: '',
state: '',
country: '',
email_address: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
gender: ['',[ Validators.required]],
marital_status: '',
contact_no: ['', [ Validators.required,  Validators.pattern("^[0-9]*$"),  Validators.minLength(10), Validators.maxLength(10)]],
date_of_birth: new Date(),
registration_date: new Date(),
time_zone: '',
blood_type: '',
country_visited: '',
citizen_status: '',
display_name: '',
supplier_name: '',
ssn_number: ''


    });
  
    if(this.edituser && this.id != null)
    {
    this.userObs = this.userService.getUser(this.id).pipe(tap(user => this.regForm.patchValue(user)))
    }
    console.log(this.userObs);
  
}

onSubmit() : void {
  if(this.regForm.valid){
    this.userDetail = this.regForm.value;
    console.log(this.regForm.value);
    console.log(this.userDetail.name);
    this.userDetail.username = localStorage.getItem('username');
    if(this.edituser){
      this.userDetail.appointments = this.existingApp;
    this.userService.putUser(this.userDetail).subscribe(user => {this.userDetail = user; console.log(this.userDetail) ;});
    }
    else{
      this.userService.postUser(this.userDetail).subscribe(user => {this.userDetail = user; console.log(this.userDetail) ; localStorage.setItem('id', JSON.stringify(this.userDetail.id))});
      
    }
window.alert('Sucessfully Registered with --> User Id: '+this.userDetail.username+' & Password: '+this.userDetail.password);
    setTimeout(() => {this.auth.login(this.userDetail);this.router.navigate(["viewappt"])}
    , 2000);

  }
  else {
    console.log('User Form Invalid');
  }
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.regForm.controls[controlName].hasError(errorName);
  }

}
