import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewapptComponent } from './viewappt.component';

describe('ViewapptComponent', () => {
  let component: ViewapptComponent;
  let fixture: ComponentFixture<ViewapptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewapptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewapptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
