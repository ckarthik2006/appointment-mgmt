import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { UserDetails } from '../shared/userDetails';
import { Appointment } from '../shared/appointment';
import { Observable } from 'rxjs';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-viewappt',
  templateUrl: './viewappt.component.html',
  styleUrls: ['./viewappt.component.scss']
})
export class ViewapptComponent implements OnInit {

  constructor(public dialog: MatDialog, private router : Router, private route : ActivatedRoute, private userService : UserService) { }

  @ViewChild(MatTable,{static:true}) table: MatTable<any>;
  
  ngOnInit(): void {
      
      console.log(localStorage.getItem('id'));
      var useridStr = localStorage.getItem('id');
      localStorage.setItem('id', useridStr);
      this.userService.getUser(localStorage.getItem('id')).subscribe(usrs => {this.userDetail = usrs;  this.appointments = this.userDetail.appointments;this.userloaded = true; console.log(usrs) ;});
      console.log(this.userDetail);
      console.log(this.appointments);
      
  }

  id : number;
  userDetail : UserDetails;
  appointments : Appointment[] = [];
  userloaded : boolean;
  userObs : Observable<UserDetails>;

  bookAppt() : void {
    console.log("Redirecting to Book Appt");
    this.router.navigate(["bookAppt"]);
  }

  updateUser() : void {
    console.log("Redirecting to Update User");
    this.router.navigate(["register"]);
  }

  displayedColumns: string[] = ['timezone', 'appointmenttype', 'startdate', 'title','starttime','endtime','location','comments','action'];

  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data:obj
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }

  updateRowData(row_obj){
    this.appointments = this.appointments.filter((value,key)=>{
      if(value.title == row_obj.title && value.timezone == row_obj.timezone){
        value.starttime = row_obj.starttime;
        value.endtime = row_obj.endtime;
        value.comments = row_obj.comments;
      }
      return true;
    });
  }
  deleteRowData(row_obj){
    this.appointments = this.appointments.filter((value,key)=>{
      return value.title != row_obj.title && value.timezone != row_obj.timezone;
    });
  }
}
