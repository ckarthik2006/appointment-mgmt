import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  hidelogout : boolean;
  loggedIn = new BehaviorSubject<boolean>(false);
  isLoggedIn$: Observable<boolean>; 
  constructor(private router : Router, private auth : AuthService) {  }

  ngOnInit(): void {
    this.hidelogout = JSON.parse(localStorage.getItem('id')) == null ? false : true ;
    console.log('Header comp');
    console.log(localStorage.getItem('id'));
    console.log(this.hidelogout);
    this.isLoggedIn$ = this.auth.isLoggedIn;
  }

  onLogout() {
    this.hidelogout = true;
    localStorage.removeItem('id');
    console.log('Redirecting to Auth Serv');
    this.auth.logout();
    
  }
  homeClick()
  {
    console.log('Redirecting to Home');
    this.router.navigate(["viewappt"]);
  }
}
