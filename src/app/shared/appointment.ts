export class Appointment {
    timezone : string;
    appointmenttype : string;
    startdate : string;
    title : string;
    starttime : Date;
    endtime : string;
    location : string;
    comments : string;
}

export class TimeOption { 
    constructor(public timeId:string, public timeName:string) {
    }	
} 