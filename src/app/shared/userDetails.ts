import { Appointment } from './appointment';

export class UserDetails {
    id: number;
    userid : number;
    name : string;
    username : string;
    password : string;
    guardian_type  : string;
    guardian_name : string;
    address : string;
    citizenship : string;
    state : string;
    country : string;
    email_address : string;
    gender : string;
    marital_status : string;
    contact_no : string;
    date_of_birth : Date;
    registration_date : Date;
    time_zone : string;
    blood_type : string;
    country_visited : string;
    citizen_status : string;
    display_name : string;
    supplier_name : string;
    ssn_number : string;
    appointments : Appointment[];
}

export class Gender {
    value : String;
    viewValue : String;
}