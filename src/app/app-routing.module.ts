import { BookapptComponent } from './bookappt/bookappt.component';
import { RegisterComponent } from './register/register.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ViewapptComponent } from './viewappt/viewappt.component';
import { AuthGuard } from './auth/auth.guard';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'viewappt', component: ViewapptComponent, canLoad: [AuthGuard]},
  { path: 'bookAppt', component: BookapptComponent },
  { path : '', component : LoginComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
