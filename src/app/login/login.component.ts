import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { UserDetails } from '../shared/userDetails';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { BehaviorSubject } from 'rxjs';
import { externaljs } from 'src/assets/javascript/external';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb : FormBuilder , private router : Router, private userservice : UserService, private auth : AuthService) { 
    localStorage.removeItem('id');
  }
  loginForm : FormGroup;
  username: string;
  password: string;
  user: UserDetails;
  loginuser : UserDetails;
  users: UserDetails[];
  id : number;
  usermatch : boolean;
  private loggedIn = new BehaviorSubject<boolean>(false);
    ngOnInit() {
      localStorage.removeItem('id');
      this.getusrsub();
      this.loginForm = this.fb.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
    })
    };

    public hasError = (controlName: string, errorName: string) =>{
      return this.loginForm.controls[controlName].hasError(errorName);
    }

    getusrsub() : void {
      this.userservice.getUsers().subscribe(usrs => this.users = usrs);
    }
    login() : void {

      if (this.loginForm.invalid) {
        console.log('form invalid')
        return;
    }

    this.useriteration();

    this.credentialValidator();
      
    }

    credentialValidator () : void {
      if(this.user.password == this.loginuser.password)
      {
        this.id = this.user.id;
        //var useridStr = JSON.stringify(this.user.userid);
        console.log('login id fetched'+JSON.stringify(this.user));
        localStorage.setItem('id', JSON.stringify(this.user.id));
        this.auth.login(this.user);
      }else {
        alert("Invalid credentials");
      }
    }

    useriteration() : void {
      this.loginuser = this.loginForm.value;
     for(let tempusr of this.users){
      if(tempusr.username == this.loginuser.username)
      {
        this.user = tempusr;
        this.usermatch = true;
        break;
      }
     }
    }
    register() : void {
      let randomid = Math.floor(100000 + Math.random() * 900000);
      console.log(randomid);
      localStorage.setItem('username',JSON.stringify(randomid));
      this.router.navigate(["register"]);
    }
    }
  