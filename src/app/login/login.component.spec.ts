import {UserDetails} from '../shared/userDetails';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from '../services/user.service';
import { RouterTestingModule } from '@angular/router/testing';
import { ViewapptComponent } from '../viewappt/viewappt.component';
import { RegisterComponent } from '../register/register.component';
import { of } from 'rxjs';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule  } from '@angular/platform-browser/animations';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let service: UserService;
  const dummyUsers: UserDetails[] = [{
    name: 'karthik1',
    password: 'demo',
    guardian_type: 'Spouse',
    guardian_name: 'XXX',
    address: 'chennai',
    citizenship: 'Indian',
    state: 'Puebla',
    country: 'Mexico',
    email_address: 'aa@ff.cc',
    gender: 'Female',
    marital_status: 'Single',
    contact_no: '1111111111',
    date_of_birth: new Date('2020-09-03T12:23:32.596Z'),
    registration_date: new Date('2020-09-03T12:23:32.596Z'),
    time_zone: '',
    blood_type: '',
    country_visited: 'USA',
    citizen_status: '',
    display_name: '',
    supplier_name: '',
    ssn_number: '',
    username: 'demo',
    userid: 19,
    id: 19,
    appointments: [
      {
        timezone: 'PST',
        appointmenttype: 'Private',
        startdate: '11-09-2020',
        title: 'Physio ',
        starttime: new Date('10:00 AM'),
        endtime: '11:00 AM',
        location: '',
        comments: ''
      }
    ]
}];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, MatFormFieldModule, NoopAnimationsModule , MatInputModule, FormsModule,HttpClientModule, HttpClientTestingModule, MatCardModule, RouterTestingModule.withRoutes([
        { path: 'viewappt', component: ViewapptComponent},
        { path: 'register', component: RegisterComponent }
    ]), RouterModule.forRoot([])],
      declarations: [ LoginComponent ],
      providers: [UserService]
    })
    .compileComponents();
    service = TestBed.get(UserService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('service should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('Form should be invalid', async(()=> {
    component.loginForm.controls['username'].setValue('');
    component.loginForm.controls['password'].setValue('');
    expect(component.loginForm.valid).toBeFalsy();
  }));

  it('Login', async(() => {
    component.login();
    let usrn = component.loginForm.controls['username'] ;
    const service: UserService = TestBed.get(UserService);
    service.getUsers().subscribe(users => {
      
      expect(users[0].username).toEqual(usrn.value);
    //expect(component.login).toBeTruthy();
    });

 }));

 it("should call getUsers and return list of users", async(() => {
  const response: UserDetails[] = [];

  spyOn(service, 'getUsers').and.returnValue(of(response))

  component.getusrsub();

  fixture.detectChanges();

  expect(component.users).toEqual(response);
}));


it('should iterate the users', () => {
  const newUsr: UserDetails[] = dummyUsers;
  //const comp: LoginComponent = TestBed.get(LoginComponent);
  component.loginuser = new UserDetails();
  component.loginuser.username = component.loginForm.controls['username'].value;
  component.users = newUsr;
  
  component.useriteration();
  fixture.detectChanges();
  expect(spyOn<any>(component, 'useriteration')).toBeTruthy();
});

it('should Validate the user credentials', () => {
  const newUsr: UserDetails[] = dummyUsers;
  //const comp: LoginComponent = TestBed.get(LoginComponent);
  component.loginuser = dummyUsers[0];
  component.loginuser.password = 'demo';
  component.user = newUsr[0];
  
  component.credentialValidator();
  fixture.detectChanges();
  expect(spyOn<any>(component, 'credentialValidator')).toBeTruthy();
});

 it('Register', async(() => {
  component.register();
  expect(component.register).toBeTruthy();

}));

  it('Form should be valid', async(()=> {
    component.loginForm.controls['username'].setValue('admin');
    component.loginForm.controls['password'].setValue('admin123');
    expect(component.loginForm.valid).toBeTruthy();
  }));

});
